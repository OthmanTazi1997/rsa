from math import *
from random import *


def twoRandHightPrime(ending):
    perm = list (range(2, ending+1))
    k=2
    nSquared = sqrt(ending)
    while k < nSquared :
        perm =[p for p in perm if p<=k or p%k != 0]
        k=perm[perm.index(k)+1]

    halfLenght = int((len(perm)/2))
    fullLenght = len(perm)

    firstIndex=randrange(halfLenght)
    secondIndex=randrange(halfLenght)
    while firstIndex == secondIndex :
        firstIndex=randrange(halfLenght)
        secondIndex=randrange(halfLenght)

    return perm[halfLenght:][firstIndex] , perm[halfLenght:][secondIndex] , perm[halfLenght:][secondIndex] * perm[halfLenght:][firstIndex]

#L = twoRandHightPrime(10000)
#print ("p= " , L[1], "   q= ",L[0], "   n= ", L[2])

def eea(a,b):
	if b==0:return (1,0)
	(q,r) = (a//b,a%b)
	(s,t) = eea(b,r)
	return (t, s-(q*t) )

# Find the multiplicative inverse of x (mod y)
def find_inverse(x,y):
	inv = eea(x,y)[0]
	if inv < 1: inv += y #we only want positive values
	return inv


def randomKeyGenerator(ending):
    L=L = twoRandHightPrime(ending)
    p=L[0]
    q=L[1]
    n=L[2]
    phi = (p-1)*(q-1)
    e = randrange(1,phi)
    g = gcd(e,phi)
    while g != 1:
        e = randrange(1,phi)
        g = gcd(e,phi)
    
    d = find_inverse(e,phi)
    #d = multiplicative_inverse(e,phi)
    #print(g)
    #print(e)
    #print(d) #none
    return ((e,n),(d,n))

def randomKeyEncrapting(pk,msg):
    e,n = pk
    cMsg= [(ord(char) ** e)%n for char in msg]
    #cMsg= [ord(char) for char in msg]
    #print(e)
    return cMsg

def randomKeyDecripting(pk,cMsg):
    e,n = pk
    msg = [chr((char ** e)%n) for char in cMsg]

    return ''.join(msg)
