from randomKey import *
from userKey import *
from utility import *

def returnToMenuAsk():
    MenuAsk = input("  do you whant to return to the Menu ? (y/n) : ")
    if MenuAsk not in ('y','n'):
        print ("  please select a valide answer (y/n)  ")
        return returnToMenuAsk()
    elif MenuAsk == 'y' :
        servicesChoice()
    else:
        return " "

def servicesChoice():
    choiceValue = giveChoices()
    if choiceValue == 1 :
        marge = int(input("input the maximume number for prime numbers : "))
        print("************* GENERATING A RANDOM KEY *************")
        public , private = randomKeyGenerator(marge)
        print("the public key is : ", "\"" , public ,"\"" ," and the private key is : " ,"\"" , private,"\"")

        msg = input("enter a msg to encript : ")
        encriptedMsg = randomKeyEncrapting(private,msg)
        print("your encrypted bmsg is :")
        print(encriptedMsg)
        #print(''.join(map(lambda x: str(x),encriptedMsg)))

        print("\n****************************************************\n")         
        print("decripting the msg using the following key :" , public)
        print("your msg is : ")
        print(randomKeyDecripting(public,encriptedMsg))
                
        print(returnToMenuAsk())

    elif choiceValue == 2 :
        p = int(input("Enter a prime number (17, 19, 23, etc): "))
        q = int(input("Enter another prime number (Not one you entered above): "))
        print ("Generating your public/private keypairs now")
        public, private = userGenerate_keypair(p, q)
        print("the public key is : ", "\"" , public ,"\"" ," and the private key is : " ,"\"" , private,"\"")

        msg = input("enter a msg to encript : ")
        encryptedMsg = userEncrypt(private, msg)
        print ("Your encrypted message is: ")
        print(encryptedMsg)
        #print ''.join(map(lambda x: str(x), encrypted_msg))

        print("\n****************************************************\n")         
        print ("Decrypting message with public key :", public)
        print ("Your msg is: ")
        print (userDecrypt(public, encryptedMsg))
                
           
        print(returnToMenuAsk())
                

print(servicesChoice())