
from math import *
from random import *


def eea(a,b):
	if b==0:return (1,0)
	(q,r) = (a//b,a%b)
	(s,t) = eea(b,r)
	return (t, s-(q*t) )

# Find the multiplicative inverse of x (mod y)
def find_inverse(x,y):
	inv = eea(x,y)[0]
	if inv < 1: inv += y #we only want positive values
	return inv

def is_prime(num):
    if num == 2:
        return True
    if num < 2 or num % 2 == 0:
        return False
    for n in range(3, int(num**0.5)+2, 2):
        if num % n == 0:
            return False
    return True

def userGenerate_keypair(p, q):
    while is_prime(p) == False or is_prime(q)==False:
       if is_prime(p) == False and is_prime(q)==False:
           print("p and q should be prime numbers !")
           p = int(input("input p: "))
           q = int(input("input q: "))
           userGenerate_keypair(p, q)
       if is_prime(p) == False:
           p = int(input ("p must be a prime number!\n input p: "))
           userGenerate_keypair(p, q)
       elif is_prime(q) == False:
           q = int(input ("p must be a prime number!\n input q:"))
           userGenerate_keypair(p, q)
       elif p == q :
           print("p and q should be different !")
           p = int(input("input p: "))
           q = int(input("input q: "))
           userGenerate_keypair(p, q)
       
    n = p*q
    phi = (p-1) * (q-1)
    e = randrange(1, phi)
    g = gcd(e,phi)
    while g != 1:
        e = randrange(1,phi)
        g = gcd(e,phi)
    
    d = find_inverse(e,phi)
    return ((e, n), (d, n))

def userEncrypt(pk, plaintext):
    key, n = pk
    cipher = [(ord(char) ** key) % n for char in plaintext]
    return cipher

def userDecrypt(pk, ciphertext):
    key, n = pk
    plain = [chr((char ** key) % n) for char in ciphertext]
    return ''.join(plain)

    



